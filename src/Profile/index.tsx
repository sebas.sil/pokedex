import React, {useEffect, useRef, useState} from 'react'
import {SafeAreaView, StatusBar, View, Text, TextInput, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, Keyboard} from 'react-native'
import styles, {colors, padding, theme} from '../styles'
import Avatar from '../Components/Avatar'
import ValueBar from '../Components/ValueBar'
import SearchIcon from '../assets/icons/search.svg'

export interface PokeData {
    id: number
    name: string
    types: Array<string>
    image: string
    status: {
        hp: number
        attack: number
        defense: number
        special_attack: number
        special_defence: number
        speed: number
    }
    description: string
    generation: string
}

const Profile = () => {

    const [pokemon, setPokemon] = useState<PokeData | undefined>(undefined)
    const [hide, setHide] = useState<boolean>(false)
    const [search, setSearch] = useState<string>('bulbasaur')
    const timerToWrite = useRef<NodeJS.Timeout>()

    useEffect(() => {
        Keyboard.addListener("keyboardDidShow", () => {
            setHide(true)
        });

        Keyboard.addListener("keyboardDidHide", () => {
            setHide(false)
        });
    }, [])

    useEffect(() => {
        if (timerToWrite.current) {
            clearTimeout(timerToWrite.current)
        }

        if(search){
            timerToWrite.current = setTimeout(() => {
                fetch('http://pokeapi.co/api/v2/pokemon/' + search.toLowerCase())
                    .then(T => T.ok ? T.json() : undefined)
                    .then(P => {
                        if (P) {
                            return {
                                id: P.id,
                                name: P.name,
                                types: P.types && (P.types.map((e: any) => e.type.name)),
                                status: P.stats.map((s: any) => ({[s.stat.name.replace('-', '_')]: s.base_stat})).reduce(((r:any, c:any) => Object.assign(r, c)), {})
                            } as PokeData
                        } else {
                            return undefined
                        }
                    })
                    .then(P => {
                        if (P) {
                            return fetch('https://pokeapi.co/api/v2/pokemon-species/' + P.id)
                                .then(T => T.ok ? T.json() : undefined)
                                .then(D => {
                                    if (D) {
                                        const en = D.flavor_text_entries.filter((e:any) => e.language.name === 'en')
                                        P.description = en ? en[0].flavor_text.replace(/\n|\f/g, ' ') : ''
                                        P.generation = D.generation.name.split('-')[1]
                                    }
                                    return P
                                }).catch(e => {
                                    console.error(e)
                                    return P
                                })
                        }
                    })
                    .then(setPokemon).catch(console.error)
            }, 1000)
        }
    }, [search])

    useEffect(() => {
        //console.log('poke', pokemon)
    }, [pokemon])


    return (
        <>
            <StatusBar backgroundColor={colors.background_in_wite} />
            <SafeAreaView style={[styles.container]}>
                <View style={[styles.primaryContainer]}>
                    <Avatar item={pokemon} />
                </View>
                <View style={[styles.secondaryContainer, hide && {flex: 0}]}>

                    <View style={[{flex: 1, flexDirection: 'row'}, hide && {display: 'none'}]}>
                        <View style={{flex: 1, paddingHorizontal: 5}}>
                            {pokemon && <>
                            <ValueBar name='Vida' value={pokemon.status.hp} type={pokemon.types[0]}/>
                            <ValueBar name='Ataque' value={pokemon.status.attack} type={pokemon.types[0]}/>
                            <ValueBar name='Defesa' value={pokemon.status.defense} type={pokemon.types[0]}/>
                            <ValueBar name='Veloc.' value={pokemon.status.speed} type={pokemon.types[0]}/>
                            </>}
                        </View>
                        <View style={{flex: 1, paddingHorizontal: 5, width: '100%'}}>
                            <Text style={{ marginBottom: 10}}>{pokemon?.description}</Text>
                            {pokemon?.types && <View style={{flexDirection: 'row'}}>
                                <Text style={{fontWeight: 'bold'}}>tipo: </Text>
                                <Text style={{}}>{pokemon?.types.join(', ')}</Text>
                            </View>}
                        </View>
                    </View>
                    <View>
                    <TextInput value={search} onChangeText={setSearch} autoCapitalize='none' style={[styles.search, {backgroundColor: theme[pokemon?.types[0]].color}]} placeholder='Nome do pokemon' />
                    <SearchIcon style={{position: 'absolute', top: 5, right: 15, color: colors.background_in_wite}} />
                    </View>
                </View>
            </SafeAreaView>
        </>
    )
}

export default Profile