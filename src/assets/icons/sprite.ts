import Svg, {SvgProps} from 'react-native-svg'
import Bug from '../icons/bug.svg'
import Dark from '../icons/dark.svg'
import Dragon from '../icons/dragon.svg'
import Electric from '../icons/electric.svg'
import Fairy from '../icons/fairy.svg'
import Fighting from '../icons/fighting.svg'
import Fire from '../icons/fire.svg'
import Flying from '../icons/flying.svg'
import Ghost from '../icons/ghost.svg'
import Grass from '../icons/grass.svg'
import Ground from '../icons/ground.svg'
import Ice from '../icons/ice.svg'
import Normal from '../icons/normal.svg'
import Poison from '../icons/poison.svg'
import Psychic from '../icons/psychic.svg'
import Rock from '../icons/rock.svg'
import Steel from '../icons/steel.svg'
import Water from '../icons/water.svg'
import Pokeball from '../icons/pokeball.svg'


const Sprite = (name:string):React.FC<SvgProps> => {
    switch(name){
        case 'bug': return Bug
        case 'dark': return Dark
        case 'dragon': return Dragon
        case 'electric': return Electric
        case 'fairy': return Fairy
        case 'fighting': return Fighting
        case 'fire': return Fire
        case 'flying': return Flying
        case 'ghost': return Ghost
        case 'grass': return Grass
        case 'ground': return Ground
        case 'ice': return Ice
        case 'normal': return Normal
        case 'poison': return Poison
        case 'psychic': return Psychic
        case 'rock': return Rock
        case 'steel': return Steel
        case 'water': return Water
        default: return Pokeball
    }
}

export default Sprite