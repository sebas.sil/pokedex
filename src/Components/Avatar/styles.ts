import {StyleSheet} from 'react-native'
import {colors, dimensions, fonts, padding} from '../../styles'

export const octogonSize = (dimensions.fullHeight / 2 - (2 * padding.sm))
const triangleSize = octogonSize * 0.26

export const icons = (index:number) => {
    switch(index) {
        case 0: return styles.icon1
        case 1: return styles.icon2
    }
}

const styles = StyleSheet.create({
    container: {
        height: dimensions.fullHeight / 2 - (2 * padding.sm),
        width: dimensions.fullHeight / 2 - (2 * padding.sm)
    },

    triangle1:{
        position: 'absolute', 
        top: 0, 
        left: 0,
        width: triangleSize,
        height: triangleSize
    },

    triangle2: {
        position: 'absolute', 
        top: 0, 
        right: 0, 
        transform: [{rotate: '90deg'}],
        width: triangleSize,
        height: triangleSize
    },

    triangle3:{
        position: 'absolute', 
        bottom: 0, 
        right: 0, 
        transform: [{rotate: '180deg'}],
        width: triangleSize,
        height: triangleSize
    },

    triangle4: {
        position: 'absolute', 
        bottom: 0, 
        left: 0, 
        transform: [{rotate: '-90deg'}],
        width: triangleSize,
        height: triangleSize
    },
    avatar: {
        alignSelf: 'center',
        position: 'absolute',
        bottom: '50%',
        marginBottom: -(octogonSize/4),
        height: 10,
        width: 10
    },

    octagon: {
    },

    name: {
        fontSize: fonts.md,
        bottom: padding.sm,
        position: 'absolute',
        alignSelf: 'center',
        textTransform: 'capitalize'
    },
    icon1: {
        width: 36, 
        height: 36, 
        position: 'absolute', 
        top: triangleSize / 8,
        left: triangleSize / 8
    },
    icon2: {
        width: 36, 
        height: 36, 
        position: 'absolute', 
        top: triangleSize / 8,
        right: triangleSize / 8,
    },
    number: {
        height: 24,
        textAlign: 'right',
        width: triangleSize, 
        fontSize: fonts.md,
        position: 'absolute', 
        lineHeight: fonts.md,
        bottom: triangleSize / 8,
        right: triangleSize / 8,
        color: colors.background_in_wite
    },
    generation: {
        height: 24,
        textAlign: 'left',
        width: triangleSize, 
        fontSize: fonts.md,
        position: 'absolute', 
        lineHeight: fonts.md,
        bottom: triangleSize / 8,
        left: triangleSize / 8,
        color: colors.background_in_wite
    }
})

export default styles