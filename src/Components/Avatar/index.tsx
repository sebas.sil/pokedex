import React, {ReactComponentElement, useEffect, useState} from 'react'
import {View, Text} from 'react-native'
import Svg, {Image, SvgProps, SvgUri, SvgXml} from 'react-native-svg'
import Sprite from '../../assets/icons/sprite'
import Octagon from '../../assets/images/octagon.svg'
import Triangle from '../../assets/images/triangle.svg'
import {theme} from '../../styles'
import {PokeData} from '../../Profile/index'

import styles, {icons, octogonSize} from './styles'

interface AvatarProps {
    item?: PokeData
}

const Avatar: React.FC<AvatarProps> = ({item}) => {

    const [img, setImg] = useState<string | undefined>()

    useEffect(() => {
        if(item){
            fetch(`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${item.id}.svg`)
            .then(T => T.ok ? T.text() : undefined)
            .then(P => {
                setImg(P)
            }).catch(e => setImg(undefined))
        } else {
            setImg(undefined)
        }
    }, [item])

    return (
        <View style={[styles.container]}>
            <Octagon style={[styles.octagon]} />
            <Text style={styles.name}>{item?.name}</Text>
            { img && <SvgXml xml={img} width={octogonSize / 2} height={octogonSize / 2} style={[styles.avatar]} />}
            <Triangle style={[styles.triangle1, item && theme[item.types[0]]]} />
            <Triangle style={[styles.triangle2, item && (item?.types[1] ? theme[item?.types[1]] : theme[item.types[0]])]} />
            <Triangle style={[styles.triangle3, item && theme[item?.types[0]]]} />
            <Triangle style={[styles.triangle4, item && (item?.types[1] ? theme[item?.types[1]] : theme[item?.types[0]])]} />

            {item?.types && item?.types.map((t, i) => {
                const Icon:React.FC<SvgProps> = Sprite(item?.types[i])
                return <Icon key={i} style={icons(i)} title={t} />
            })}

            <Text style={styles.number}>#{item?.id}</Text>
            <Text style={styles.generation}>{item?.generation.toUpperCase()} Gen</Text>

        </View>
    )
}

export default Avatar