import React from 'react'
import {View, Text} from 'react-native'
import {colors, theme} from '../../styles'

interface ValueBarProps {
    name: string
    value: number
    type: string
}

const ValueBar:React.FC<ValueBarProps> = ({name, value, type}) => {
    value = value / 10
    value = value > 10 ? 10 : value
    return (
        <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: 5}}>
            <Text style={{flex: 1}}>{name}</Text>
            <View style={{position: 'absolute', borderBottomWidth: 1, borderColor: colors.gray_bar, opacity: 0.5, width: 110, top: 20, right: 0}} />
            {new Array(Math.floor(value)).fill(undefined).map((e,i) => {
                return <View key={i} style={{height: 40, width: 10, borderLeftWidth: 3, borderColor: theme[type]?.color}} />
            })}
            {value % 1 > 0 &&
                <View style={{height: 40, width: 10, justifyContent: 'center'}}>
                    <View style={{height: 40, width: 10, borderLeftWidth: 1, borderColor: colors.gray_bar, position: 'absolute', opacity: 0.5}} />
                    <View style={{height: ((value % 10) * 4), width: 10, borderLeftWidth: 3, borderColor: theme[type].color, marginLeft: -1}} />
                </View>
            }
            {(value % 1 === 0 ? (value = value) : (value += 1)) &&
            new Array(Math.ceil(10 - value)).fill(undefined).map((e,i) => {
                return <View key={i} style={{height: 40, width: 10, borderLeftWidth: 1, borderColor: colors.gray_bar, opacity: 0.5}} />
            })}
        </View>
    )
}

export default ValueBar