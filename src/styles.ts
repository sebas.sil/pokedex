import {StyleSheet, Dimensions} from 'react-native'

export const colors = {
    background_in_wite: '#FAFAFA',
    color_type_psychic: '#FF6675',
    content_text: '#6A6180',
    gray_bar: '#6A6180'
}

export const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
}

export const theme:{ [index:string] : StyleSheet.NamedStyles<object> } = {

    water: {
        color: '#3692DC'
    },
    flying: {
        color: '#89AAE3'
    },
    normal: {
        color: '#919AA2'
    },
    dragon: {
        color: '#006FC9'
    },
    rock: {
        color: '#C8B686'
    },
    poison: {
        color: '#B567CE'
    },
    ice: {
        color: '#4CD1C0'
    },
    electric: {
        color: '#FBD100'
    },
    fairy: {
        color: '#FB89EB'
    },
    ghost: {
        color: '#4C6AB2'
    },
    steel: {
        color: '#5A8EA2'
    },
    ground: {
        color: '#E87236'
    },
    dark: {
        color: '#5B5466'
    },
    fighting: {
        color: '#E0306A'
    },
    bug: {
        color: '#83C300'
    },
    fire: {
        color: '#FF9741'
    },
    psychic: {
        color: '#FF6675'
    },
    grass: {
        color: '#709E5D'
    },
    undefined: {
        
    }
}

export const padding = {
    /**
     * 15
     */
    sm: 15
}

export const fonts = {
    /**
     * 9
     */
    sm: 9,
    /**
     * 18
     */
    md: 18,
    /**
     * 36
     */
    lg: 36,
    /**
     * 72
     */
    xl: 72,
    /**
     * Poppins-Regular
     */
    primary: 'Poppins-Regular',
    /**
     * Poppins-Bold
     */
    primaryBold: 'Poppins-Bold',
    /**
     * Archivo-Regular
     */
    secondary: 'Archivo-Regular',
    /**
     * Archivo-Bold
     */
    secondaryBold: 'Archivo-Bold'
  }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: padding.sm,
        backgroundColor: colors.background_in_wite
    },

    primaryContainer: {
        flex: 1,
        borderBottomWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    secondaryContainer: {
        flex: 1,
        paddingTop: 5
    },

    search: {
        height: 54,
        backgroundColor: 'white',
        borderRadius: 8,
        justifyContent: 'center',
        paddingHorizontal: padding.sm / 2,
        borderColor: 'black',
        borderWidth: 1,
        width: '100%',
        color: colors.background_in_wite
    }
})

export default styles
