import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import Profile from './src/Profile'

declare const global: {HermesInternal: null | {}};

const App = () => {
  return (
    <Profile />
  );
};

export default App;
