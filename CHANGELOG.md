# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Text in Portuguese
- Better code segregation
- Type of precedence (which type wins)
- Pokemon sound (voice)
- Better layout for small devices
- ML to recognize pokemon by image
- 3D modeling of a pokemon to show it moving

## [1.0.0]

### Added
- Pokemon profile with status and description
- Serarch pokemon by name

[1.0.0]: https://gitlab.com/sebas.sil/pokedex/-/releases/1.0.0