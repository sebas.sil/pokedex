# Pokedex

React-native project to simulate an pokedex, an device to get pokemon informations. This code use a public API [pokeapi.co](https://pokeapi.co).

### Get Started

1. Intall `Yarn`: https://yarnpkg.com/getting-started/install
2. After cloning this repository run `yarn install` to intall all the dependencies

### Deploy / Run

To execute this frontend run `yarn start`. This will run the react-native lib system. You need to connect your phone.

### Built With

[Visual Code 1.48.1](https://code.visualstudio.com/Download) - IDE

### Main dependencies

- [react](https://www.npmjs.com/package/react): JavaScript library for creating user interfaces.
- [react-native](https://www.npmjs.com/package/react-native): React's declarative UI framework to iOS and Android

### directory layout structure

    .
    ├── .gitimages          # repository images to show on README files
    ├── src                 # main folder containing all app components and codes
    │   ├── assets          # app resources folder
    │   ├── Componentes     # shared components. A component is 'shared' whe it is used in more than one screen
    │   └── Profile         # Pokemon profile screen
    └── ...                 # others app files to configure this repository and project

### Resources

Figma Prototipe: Figma is an online design tool. It was used to create a medium fidelity prototype, based on the [previous](.gitimages/prot_baixa_fidelidade.jpeg) (low fidelity prototype) and can be accessed at: https://www.figma.com/file/Bq1JSc8dCpNX3qwuxy47Ru/pokedex-1.0


#### Requisites:

---

- **RF01** - Display the pokemon's profile with the information: name, description, type, image, and status
- **FR02** - Search for a specific pokemon

---

- **RNF01** - All images must be in SVG format
Images can be acquire at: https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/<id\>.svg
In this location there are no images of generation 8
- **RNF02** - The name of the pokemon must be formatted as a proper name (first capital letters)
The name comes from the "name" property of the API "pokemon" resource
- **RNF03** - The type must be presented as an image and written
The type images can be acquire at: https://commons.wikimedia.org/w/index.php?search=pokemon+type&title=Special%3ASearch&go=Go&ns0=1&ns6=1&ns12=1&ns14=1&ns100=1&ns106=1]
The type comes from the "types" property of the API's "pokemon" resource
- **RNF04** - The description of the pokemon must be in Portuguese
The description, acquired from https://pokeapi.co/api/v2/pokemon-species/<id\>, in the property "flavor_text_entries", is not in Portuguese. A translation will be required (unlikely)
- **RNF05** - The status must contain the information: amount of attack, defense, health and speed
The status comes from the "stats" property of the API "pokemon" resource
- **RNF06** - The name search must not take into account the case

---

### Running example images

<table>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/pikachu.jpg" alt="pikachu profile" width="240" />
                <figcaption>pikachu profile</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/bulbasaur.jpg" alt="bulbasaur profile" width="240" />
                <figcaption>bulbasaur profile</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/rattata.jpg" alt="rattata profile (search)" width="240" />
                <figcaption>search of rattata profile</figcaption>
            </figure>
        </td>
    </tr>
</table>